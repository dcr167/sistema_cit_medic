$(obtener_registros());

function obtener_registros(citas)
{
	$.ajax({
		url : 'tabla_citas.php',
		type : 'POST',
		dataType : 'html',
		data : { citas: citas },
		})

	.done(function(resultado){
		$("#tabla_resultado").html(resultado);
	})
}

$(document).on('keyup', '#busqueda', function()
{
	var valorBusqueda=$(this).val();
	if (valorBusqueda!="")
	{
		obtener_registros(valorBusqueda);
	}
	else
		{
			obtener_registros();
		}
});
