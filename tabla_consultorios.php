<!DOCTYPE html>
<html>
    <head>
        <title>Ver|Consultorios</title>
        
        <link rel="stylesheet" type="text/css" media="screen" href="css/st_tabla_consultorios.css" />
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" href="css/fontello.css">
        
    
    
    </head>
    <body>



        <div id="main">
            <table>
                <thead>
                    <tr>
                        <th colspan="1"><a href="c_rconsultorios.php">Nuevo</a></th>
                        <th colspan="3">Lista de consultorios</th>
                    
                    </tr>
                
                </thead>
                    <?php
                    include("connect_rconsultorios.php");

                    $tabla="";
$query="SELECT * FROM consultorios ORDER BY num_consultorio";

///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////
if(isset($_POST['consultorios']))
{
	$q=$conexion->real_escape_string($_POST['consultorios']);
	$query="SELECT * FROM consultorios WHERE 
		num_consultorio LIKE '%".$q."%' OR
		id_consultorio LIKE '%".$q."%' OR
		nombre_consultorio LIKE '%".$q."%'";
}

$buscarConsultorios=$conexion->query($query);
if ($buscarConsultorios->num_rows > 0)
{
	$tabla.= 
	'<table class="table">
		<tr class="bg-primary">
			<td class="had">Nº</td>
			<td class="had">ID</td>
			<td class="had">Nombre</td>
            <td colspan="2" class="had">Operaciones</td>
		</tr>';

	while($filaConsultorios= $buscarConsultorios->fetch_assoc())
	{
		$tabla.=
		'<tr class="bg-danger">
			<td class="">'.$filaConsultorios['num_consultorio'].'</td>
			<td>'.$filaConsultorios['id_consultorio'].'</td>
			<td>'.$filaConsultorios['nombre_consultorio'].'</td>
            <td><a href="modificar_consultorios.php?num_consultorio='.$filaConsultorios['num_consultorio'].'" class="icon-address-book"></a></td>
            <td><a href="eliminar_consultorios.php?num_consultorio='  .$filaConsultorios['num_consultorio'].'" class="icon-trash-1"></a></td>
		 </tr>
		';
	}

	$tabla.='</table>';
} else
	{
		$tabla="<h2>No se encontraron coincidencias con sus criterios de búsqueda.</h2>";
	}


echo $tabla;

                        
                    ?>

            
            </table>
            </div>
        
        
    
  
    
    
    </body>

</html>