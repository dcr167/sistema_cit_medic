<?php
/////// CONEXIÓN A LA BASE DE DATOS /////////
$host = 'localhost';
$basededatos = 'citasmedicas';
$usuario = 'root';
$contraseña = '';

$conexion = new mysqli($host, $usuario,$contraseña, $basededatos);
if ($conexion -> connect_errno)
{
	die("Fallo la conexion:(".$conexion -> mysqli_connect_errno().")".$conexion-> mysqli_connect_error());
}

//////////////// VALORES INICIALES ///////////////////////

$tabla="";
$query="SELECT * FROM doctores ORDER BY num_doctor";

///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////
if(isset($_POST['doctores']))
{
	$q=$conexion->real_escape_string($_POST['doctores']);
	$query="SELECT * FROM doctores WHERE 
		num_doctor LIKE '%".$q."%' OR
		nombre_doctor LIKE '%".$q."%' OR
		apellido_doctor LIKE '%".$q."%' OR
		ci_doctor LIKE '%".$q."%' OR
        edad_doctor LIKE '%".$q."%' OR
		sexo_doctor LIKE'%".$q."%' OR
        telefono_doctor LIKE'%".$q."%' OR
        correo_doctor LIKE'%".$q."%' OR    
		direccion_doctor LIKE '%".$q."%'";
}

$buscarAlumnos=$conexion->query($query);
if ($buscarAlumnos->num_rows > 0)
{
	$tabla.= 
	'<table class="table">
		<tr class="bg-primary">
			<td>ID ALUMNO</td>
			<td>NOMBRE</td>
			<td>CARRERA</td>
			<td>GRUPO</td>
            <td>NOMBRE</td>
			<td>CARRERA</td>
			<td>GRUPO</td>
            <td>GRUPO</td>

		</tr>';

	while($filaAlumnos= $buscarAlumnos->fetch_assoc())
	{
		$tabla.=
		'<tr>
			<td>'.$filaAlumnos['num_doctor'].'</td>
            <td>'.$filaAlumnos['nombre_doctor'].'</td>
			<td>'.$filaAlumnos['apellido_doctor'].'</td>
			<td>'.$filaAlumnos['ci_doctor'].'</td>
			<td>'.$filaAlumnos['edad_doctor'].'</td>
			<td>'.$filaAlumnos['sexo_doctor'].'</td>
            <td>'.$filaAlumnos['telefono_doctor'].'</td>
			<td>'.$filaAlumnos['correo_doctor'].'</td>
			<td>'.$filaAlumnos['direccion_doctor'].'</td>

    
		 </tr>
		';
	}

	$tabla.='</table>';
} else
	{
		$tabla="No se encontraron coincidencias con sus criterios de búsqueda.";
	}


echo $tabla;
?>
