<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" type="text/css" href="css/st_rcitas.css">
</head>
<body>

<section class="title">
		<a href="inicio.php"><h1 class="icon-medkit">Citas Medicas</h1></a>
	</section>
	<nav class="nave">
		<ul class="menu">
			<li class="first-item">
				<a href="inicio.php">
                    <div class="ini">
                    <label for="" class="icon-home" ></label>
                    </div>
                      
					<span class="text-item">Inicio</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_citas.php">
                    <div class="h">
                <label for="" class="icon-heartbeat"></label>
                </div>
					
					<span class="text-item">Citas</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_pacientes.php">
                    <div class="pc">
                <label for="" class="icon-bed" ></label>
                </div>
					
					<span class="text-item">Pacientes</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_doctores.php">
                    <div class="doc">
                <label for="" class="icon-user-md" ></label>
                </div>
				
					<span class="text-item">Doctores</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_consultorios.php">
                    <div class="c">
                    <label for="" class="icon-hospital"></label>
                    </div>
					<span class="text-item">Consultorios</span>
					<span class="down-item"></span>
				</a>
			</li>
			

			
		</ul>

	</nav>


	<div class="fr">
		<h1>Registrar Cita</h1>
		<form action="dbsave_rcitas.php" method="POST">
			<label for="nombrepaciente">Nombre del paciente</label>
            <input type="text" name="nombrepaciente">
            <label for="nombredoctor">Nombre del doctor</label>
            <input type="text" name="nombredoctor">
            <label for="idconsultorio">ID del consultorio</label>
			<input type="text" name="idconsultorio">
            <label for="hora">Hora</label>
            <input type="time" name="hora">
            <label for="fecha">Fecha</label>
            <input type="date" name="fecha">
			<input type="submit" value="Aceptar">

			
		</form>
	</div>
	
</body>
</html>