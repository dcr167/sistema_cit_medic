<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Citas|Medicas</title>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/st_doctores.css">
    <link rel="stylesheet" href="css/fontello.css">
</head>
<body>
	<section class="title">
		<a href="inicio.php"><h1 class="icon-medkit">Citas Medicas</h1></a>
	</section>
	<nav class="nave">
		<ul class="menu">
			<li class="first-item">
				<a href="inicio.php">
                    <div class="ini">
                    <label for="" class="icon-home" ></label>
                    </div>
                      
					<span class="text-item">Inicio</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_citas.php">
                    <div class="h">
                <label for="" class="icon-heartbeat"></label>
                </div>
					
					<span class="text-item">Citas</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_pacientes.php">
                    <div class="pc">
                <label for="" class="icon-bed" ></label>
                </div>
					
					<span class="text-item">Pacientes</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_doctores.php">
                    <div class="doc">
                <label for="" class="icon-user-md" ></label>
                </div>
				
					<span class="text-item">Doctores</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_consultorios.php">
                    <div class="c">
                    <label for="" class="icon-hospital"></label>
                    </div>
					<span class="text-item">Consultorios</span>
					<span class="down-item"></span>
				</a>
			</li>
			
		</ul>

	</nav>


	<h1 id="doct">Doctores</h1>

	<a href="c_rdoctores.php">

    <div class="img">
    <div class="info">
      <p class="hdline"> Agregar Doctores</p>
      <p class="info2"> Dale click para registrar a los doctores</p>
	</div>
  	</div>
	</a>
	<a href="ver_tabla_doctores.html">
  	<div class="img">
    <div class="info">
      <p class="hdline"> Ver Doctores</p>
      <p class="info2"> Dale click para ver, editar y eliminar a los doctores</p>
      
	</div>
	</a>
	
</body>
</html>