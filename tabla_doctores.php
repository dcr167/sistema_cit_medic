<!DOCTYPE html>
<html>
    <head>
        <title>Ver|Doctores</title>
        
        <link rel="stylesheet" type="text/css" media="screen" href="css/st_tabla_pacientes_doctores.css" />
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" href="css/fontello.css">
        
    
    
    </head>
    <body>



        <div id="main">
            <table>
                <thead>
                    <tr>
                        <th colspan="1"><a href="c_rdoctores.php">Nuevo</a></th>
                        <th colspan="9">Lista de doctores</th>
                    
                    </tr>
                
                </thead>
                    <?php
               /////// CONEXIÓN A LA BASE DE DATOS /////////
                   $host = 'localhost';
                   $basededatos = 'citasmedicas';
                   $usuario = 'root';
                   $contraseña = '';
                   
                   $conexion = new mysqli($host, $usuario,$contraseña, $basededatos);
                   if ($conexion -> connect_errno)
                   {
                       die("Fallo la conexion:(".$conexion -> mysqli_connect_errno().")".$conexion-> mysqli_connect_error());
                   }

                    $tabla="";
$query="SELECT * FROM doctores ORDER BY num_doctor";

///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////
if(isset($_POST['doctores']))
{
	$q=$conexion->real_escape_string($_POST['doctores']);
	$query="SELECT * FROM doctores WHERE 
		num_doctor LIKE '%".$q."%' OR
		nombre_doctor LIKE '%".$q."%' OR
		apellido_doctor LIKE '%".$q."%' OR
		ci_doctor LIKE '%".$q."%' OR
		edad_doctor LIKE'%".$q."%' OR
        sexo_doctor LIKE '%".$q."%' OR
		telefono_doctor LIKE '%".$q."%' OR
		correo_doctor LIKE'%".$q."%' OR
        direccion_doctor LIKE'%".$q."%'" ;
}

$buscarDoctores=$conexion->query($query);
if ($buscarDoctores->num_rows > 0)
{
	$tabla.= 
	'<table class="table">
		<tr class="bg-primary">
			<td class="had">Nº</td>
			<td class="had">Nombre</td>
			<td class="had">Apellido</td>
			<td class="had">CI</td>
			<td class="had">Edad</td>
            <td class="had">Sexo</td>
            <td class="had">Telefono</td>
			<td class="had">Correo</td>
            <td class="had">Direccion</td>
            <td colspan="2" class="had">Operaciones</td>
		</tr>';

	while($filaDoctores= $buscarDoctores->fetch_assoc())
	{
		$tabla.=
		'<tr class="bg-danger">
			<td class="">'.$filaDoctores['num_doctor'].'</td>
			<td>'.$filaDoctores['nombre_doctor'].'</td>
			<td>'.$filaDoctores['apellido_doctor'].'</td>
			<td>'.$filaDoctores['ci_doctor'].'</td>
			<td>'.$filaDoctores['edad_doctor'].'</td>
            <td>'.$filaDoctores['sexo_doctor'].'</td>
            <td>'.$filaDoctores['telefono_doctor'].'</td>
			<td>'.$filaDoctores['correo_doctor'].'</td>
    		<td>'.$filaDoctores['direccion_doctor'].'</td>
            <td><a href="modificar_doctores.php?num_doctor='.$filaDoctores['num_doctor'].'" class="icon-address-book"></a></td>
            <td><a href="eliminar_doctores.php?num_doctor='  .$filaDoctores['num_doctor'].'" class="icon-trash-1"></a></td>
		 </tr>
		';
	}

	$tabla.='</table>';
} else
	{
		$tabla="<h2>No se encontraron coincidencias con sus criterios de búsqueda.</h2>";
	}


echo $tabla;

                        
                    ?>

            
            </table>
            </div>
        
        
    
  
    
    
    </body>

</html>