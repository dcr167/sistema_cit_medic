<!DOCTYPE html>
<html>
    <head>
        <title>Ver|Citas</title>
        
        <link rel="stylesheet" type="text/css" media="screen" href="css/stmodificar_doctores.css" />
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
	    <link rel="stylesheet" href="css/fontello.css">
    
    </head>
    <body>
    <section class="title">
		<a href="inicio.php"><h1 class="icon-medkit">Citas Medicas</h1></a>
	</section>
	<nav class="nave">
		<ul class="menu">
			<li class="first-item">
				<a href="inicio.php">
                    <div class="ini">
                    <label for="" class="icon-home" ></label>
                    </div>
                      
					<span class="text-item">Inicio</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_citas.php">
                    <div class="h">
                <label for="" class="icon-heartbeat"></label>
                </div>
					
					<span class="text-item">Citas</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_doctors.php">
                    <div class="pc">
                <label for="" class="icon-bed" ></label>
                </div>
					
					<span class="text-item">doctors</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_doctores.php">
                    <div class="doc">
                <label for="" class="icon-user-md" ></label>
                </div>
				
					<span class="text-item">Doctores</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_consultorios.php">
                    <div class="c">
                    <label for="" class="icon-hospital"></label>
                    </div>
					<span class="text-item">Consultorios</span>
					<span class="down-item"></span>
				</a>
			</li>
			

			
		</ul>

	</nav>


        
   
                    <?php
                    $num_doctor=$_REQUEST['num_doctor'];
                    include("connect_rdoctores.php");
                    $query="SELECT * FROM doctores WHERE num_doctor='$num_doctor'";
                    $resultado=$conexion->query($query);
                    $row=$resultado->fetch_assoc();
                        
                    ?>
            <div class="fr">
            <h1>Modificar Doctor</h1>
            <form action="modificarproceso_doctores.php?num_doctor=<?php echo $row['num_doctor']; ?>" method="POST">

            <label for="nombres">Nombres</label>
            <input type="text" name="nombres_doc"  value="<?php echo $row['nombre_doctor'];?>"required/>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos_doc"  value="<?php echo $row['apellido_doctor'];?>"required/>
            <label for="ci">CI</label>
            <input type="text" name="ci_doc"  value="<?php echo $row['ci_doctor'];?>"required/>
            <label for="edad">Edad</label>
            <input type="text" name="edad_doc"  value="<?php echo $row['edad_doctor'];?>"required/>
            <label for="sexo">Sexo</label>
            <select name="sexo_doc" id="#" value="<?php echo $row['sexo_doctor'];?>"required/>
				<option value="Masculino">Masculino</option>
				<option value="Femenino">Femenino</option>
			</select>
           
            <label for="telefono">Telefono</label>
            <input type="text" name="telefono_doc"  value="<?php echo $row['telefono_doctor'];?>"required/>
            <label for="correo">Correo</label>
            <input type="text" name="correo_doc"  value="<?php echo $row['correo_doctor'];?>"required/>
            <label for="direccion">Direccion</label>
            <input type="text" name="direccion_doc" value="<?php echo $row['direccion_doctor'];?>"required/>
            
            <input type="submit" value="Guardar">
            </form>
            </div>
            
        
        
    
    
    </body>

</html>